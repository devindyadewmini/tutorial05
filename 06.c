#include <stdio.h>

int arrsum(int arr[], int start, int len);

int main()
{
    int arr[100];
    int num, i;

    printf("Enter size of the Array: ");
    scanf("%d", &num);
    printf("\n\nEnter %d elements in the Array: \n",num);
    for(i=0; i<num; i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("\nSum of elements in the array: %d \n\n",arrsum(arr, 0, num));

    return 0;
}

int arrsum(int arr[], int start, int len)
{
    if(start >= len)
        return 0;
    else
        return (arr[start] + arrsum(arr,start + 1, len));
}



