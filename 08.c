//Print elements of array (Without using recursion)
#include <stdio.h>

int main()
{
    int arr[100];
    int i, N;

    printf("Enter size of array: ");
    scanf("%d", &N);

    printf("\nEnter %d elements : \n", N);
    for(i=0; i<N; i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("\nElements in array are: ");
    for(i=0; i<N; i++)
    {
        printf(" %d  ", arr[i]);
    }
    printf("\n\n");
    return 0;
}
