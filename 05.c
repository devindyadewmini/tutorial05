#include <stdio.h>

int hcf(int, int);
int lcm(int, int);

int lcm(int n1,int n2)
{
      if (n2 != 0)
        return ((n1 * n2)/hcf(n1,n2));
    else
        return 0;
}

int hcf(int n1, int n2)
{
    if (n2 != 0)
        return hcf(n2, n1 % n2);
    else
        return n1;
}

int main() {
    int n1, n2;
    printf("Enter positive integer: ");
    scanf("%d", &n1);
    printf("Enter positive integer: ");
    scanf("%d", &n2);
    printf("\nLCM of %d and %d is = %d \n", n1, n2, lcm(n1, n2));
    return 0;
}


