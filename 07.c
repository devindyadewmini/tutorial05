#include <stdio.h>

int maximum(int array[], int index, int len);
int minimum(int array[], int index, int len);

int maximum(int array[], int index, int len)
{
    int max;
    if(index >= len-2)
    {
        if(array[index] > array[index + 1])
            return array[index];
        else
            return array[index + 1];
    }

    max = maximum(array, index + 1, len);

    if(array[index] > max)
        return array[index];
    else
        return max;
}

int minimum(int array[], int index, int len)
{
    int min;

    if(index >= len-2)
    {
        if(array[index] < array[index + 1])
            return array[index];
        else
            return array[index + 1];
    }

    min = minimum(array, index + 1, len);

    if(array[index] < min)
        return array[index];
    else
        return min;
}

int main()
{
    int array[100], n, max, min;
    int i;

    printf("Enter size of the array: ");
    scanf("%d", &n);
    printf("\nEnter %d elements : \n", n);
    for(i=0; i<n; i++)
    {
        scanf("%d", &array[i]);
    }

    max = maximum(array, 0, n);
    min = minimum(array, 0, n);

    printf("\nMAXIMUM Value in array: %d\n", max);
    printf("MINIMUM Value in array: %d\n", min);

    return 0;
}

