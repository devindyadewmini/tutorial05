//Find maximum and minimum element in an array (Without using recursions)
#include <stdio.h>

int main()
{
    int n, num, i, max, min;
    printf("Enter the size of array: ");
    scanf("%d",&n);
    printf("\n\nEnter the elements of array : \n");
    scanf("%d",&num);
    max = num;
    min = num;
    for (i = 2; i <= n; i++)
    {
        scanf("%d",&num);
        if (num > max)
            max = num;
        if (num < min)
            min = num;
    }
    printf("\n\nThe MAXIMUM number is = %d\n", max);
    printf("The MINIMUM number is = %d", min);
    printf("\n\n");
    return 0;
}
