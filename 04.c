#include <stdio.h>

unsigned long long int fac(int);

unsigned long long int fac(int n)
{
    if(n >= 1)
        return n*fac(n-1);
    else
        return 1;
}

int main()
{
    int n;
    unsigned long long int tot;

    printf("Enter number to find Factorial >>> ");
    scanf("%d", &n);

    tot = fac(n);

    printf("\n\nFactorial of %d = %llu\n\n", n, tot);

    return 0;
}

