#include <stdio.h>

int sumn(int);

int sumn(int n)
{
    if(n == 0)
        return 0;

    return ((n % 10) + sumn(n / 10));
}

int main()
{
    int n, tot;

    printf("Enter number to find Sum of Digits >>> ");
    scanf("%d", &n);

    tot = sumn(n);

    printf("\n\nSum of digits of %d = %d\n\n", n, tot);

    return 0;
}
