#include <stdio.h>

void array(int arr[], int start, int len);

int main()
{
    int arr[100];
    int num, i;

    printf("Enter size of the Array: ");
    scanf("%d", &num);
    printf("\nEnter %d elements : \n",num);
    for(i=0; i<num; i++)
    {
        scanf("%d", &arr[i]);
    }


    printf("\n\nAll the Negative Elements in the array are: ");
    array(arr, 0, num);
    printf("\n\n");
    return 0;
}

void array(int arr[], int start, int len)
{

    if(start >= len)
        return;
        if (arr[start]<0)
        {
            printf(" %d  ", arr[start]);
        }

        array(arr, start + 1, len);
}
