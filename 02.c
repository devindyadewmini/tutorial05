#include <stdio.h>

int sumofEO(int, int);

int sumofEO(int low, int up)
{
    if(low > up)
        return 0;
    else
        return (low + sumofEO(low + 2, up));
}

int main()
{
    int low, up, sum;

    printf("Enter lower limit: ");
    scanf("%d", &low);
    printf("Enter upper limit: ");
    scanf("%d", &up);

    if (low%2==0)
    {
        printf("\n\nSum of even numbers between %d to %d = %d", low, up, sumofEO(low, up));
        printf("\nSum of odd numbers between %d to %d = %d\n\n", low, up, sumofEO(low + 1, up));
    }
    else
    {
        printf("\n\nSum of odd numbers between %d to %d = %d", low, up, sumofEO(low, up));
        printf("\nSum of even numbers between %d to %d = %d\n\n", low, up, sumofEO(low + 1, up));
    }

    return 0;
}
