//Find sum of all array elements (Without using recursions)
#include <stdio.h>
int sum(int arr[],int);

int sum(int array[],int size)
{
    int i,sum = 0;
    for(i=0; i<=size; i++)
    {
        sum+=array[i];
    }
    return sum;
}
int main()
{
  int size,i,total,arr[10];
  printf("Enter the size of array : ");
  scanf("%d",&size);
  printf("\nEnter the elements of array : \n");
  for(i = 0; i<size; i++)
  {
      scanf("%d",&arr[i]);
  }
  total = sum(arr,size);
  printf("\n\nThe sum of all the elements is: %d\n",total);
  return 0;

}
